# qudo-ml-ops-tech-task

## Setup Environment

`conda env create --file environment.yml`

`conda activate test-env`

## Running this pipeline locally

1) Activate environment.
2) Run the main.py file in the root directory.

## Tech Test Instructions

### Task: Use AWS to put this part of the data science process into a pipeline.

The weighting code takes in survey respondents from a particular country and calculates RIM/Rake weights per respondent using standard python libraries. Your task is to use any AWS pipelines (such as Sagemaker), and/or API encapsulation of the model with the goal to make this model productionizable, where it will fit into a more extensive software pipeline. 

### Things tested:

1) Your knowledge of AWS tools and infrastructure.
2) Your ability to speed up the process in any way possible.
3) Your ability to take a data science output and expose it via robust cloud tools so the engineering team can use it within the platform.
4) Your ability to refactor code.
5) Your ability to suggest tests (unit tests, integration tests etc).
6) Any of the above you do not have time to do, focus on what you think is important and will showcase your skills and ability to answer questions you think are important (that we might not have asked). For the things you don’t have time to do prepare a paragraph on what you would do if you had more time.

### Output (either):

1) Put this on your own AWS account and run us through a demo when it suits you. 
2) Give us instructions on how to host this ourselves (if you do not want to use up your own credits).

