import pandas as pd
from modules.response_processing import clean_data, add_weights


def load_data():
    return pd.read_csv("data/input.csv", low_memory=False)


def preprocess(response_data):
    return clean_data.main(response_data)


def weight(response_data):
    return add_weights.main(response_data)


def save_data(processed_data):
    processed_data.to_csv("data/output.csv")


if __name__ == '__main__':

    data = load_data()
    p = preprocess(data)
    w = weight(p)
    save_data(w)
