switch = {
    "research_name": "tech-test-wave2",
    "id_column": "ID",
    "status_col": "Status",
    "country_col": "Country",
    "region_col": "REGION",
    "gender_col": "DEM_WW_GENDER_RB_L_v3_14072020_TGT",
    "age_col": "DEM_WW_AGE_DM_L_v1_14072020_TGT",
    "desired_countries": ["United States", "Porto Rico"],
    "text_cols": ["AIDA_WW_UUBA_TB_18062021 ", "AIDA_WW_UBA_TB_07062021"],
    "target_country": "USA",
    "wave": "wave_2",
    "weighting": True,
}

