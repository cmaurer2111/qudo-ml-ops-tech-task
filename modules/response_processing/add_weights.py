import pandas as pd
from modules.master_switch import switch
from modules.response_processing.weights import weight_df
from modules.response_processing.weighting_targets import weighting_targets


def pool_states(data: pd.DataFrame, states_col: str) -> pd.DataFrame:
    midwest = ['Illinois', 'Indiana', 'Michigan', 'Ohio', 'Wisconsin', 'Iowa', 'Kansas', 'Minnesota', 'Missouri',
               'Nebraska', 'North Dakota', 'South Dakota']
    northeast = ['Connecticut', 'Maine', 'Massachusetts', 'New Hampshire', 'Rhode Island', 'Vermont', 'New Jersey',
                 'New York', 'Pennsylvania']
    south = ['Delaware', 'Florida', 'Georgia', 'Maryland', 'North Carolina', 'South Carolina', 'Virginia',
             'District of Columbia', 'West Virginia', 'Alabama', 'Kentucky', 'Mississippi', 'Tennessee', 'Arkansas',
             'Louisiana', 'Oklahoma', 'Texas', 'Puerto Rico', 'U.S. Virgin Islands']
    west = ['Arizona', 'Colorado', 'Idaho', 'Montana', 'Nevada', 'New Mexico', 'Utah', 'Wyoming', 'Alaska',
            'American Samoa', 'California', 'Hawaii', 'Oregon', 'Washington', 'Guam']

    states = []
    for item in data[states_col]:
        if item in midwest:
            states.append('Midwest')
        if item in northeast:
            states.append('Northeast')
        if item in south:
            states.append('South')
        if item in west:
            states.append('West')

    data['REGION'] = states

    return data


def drop_util_weights_cols(data: pd.DataFrame) -> pd.DataFrame:
    # remove qp_columns from weighting procedures
    qp_columns = [item for item in data.columns if 'qp_' in item]
    data.drop(columns=qp_columns, axis=1, inplace=True)
    # remove '@1' column
    data.drop(columns=['@1'], axis=1, inplace=True)

    return data


def main(data: pd.DataFrame) -> pd.DataFrame:

    if switch['target_country'] == "USA":
        states_col = [item for item in data.columns if ('state' in item.lower()) and ('dem' in item.lower())][0]
        data = pool_states(data=data,
                           states_col=states_col)
        region_col = 'REGION'

    else:
        region_col = switch['region_col']

    targets = weighting_targets(gender_col=switch['gender_col'],
                                age_col=switch['age_col'],
                                region_col=region_col)

    data = weight_df(
        data=data,
        research_name=switch['research_name'],
        weight_name="weight",
        unique_key=switch['id_column'],
        targets=targets[switch['target_country']],
        verbose=True,
    )

    data = drop_util_weights_cols(data=data)

    return data
