import pandas as pd
import re
import unidecode
from modules.master_switch import switch


def drop_missing_ID_respondents(data: pd.DataFrame) -> pd.DataFrame:
    data = data[~data.ID.isna()]
    return data


def drop_duplicates(data: pd.DataFrame, id_column: str) -> pd.DataFrame:
    print('Check duplications within response data ...\n')
    if data[id_column].nunique() != data.shape[0]:
        print(f'     - Duplicates were detected in {id_column} column! Dropping duplicates and keeping first ...\n')
        data = data.drop_duplicates(subset=id_column, keep='first')
    else:
        print(f"     - No duplicates detected in {id_column} column.\n")
    return data


def drop_incompletes(data: pd.DataFrame, status_col: str) -> pd.DataFrame:
    data = data[data[status_col] == 'Complete'].copy(deep=True)
    return data


def drop_unwanted_cols(data: pd.DataFrame) -> pd.DataFrame:
    unwanted_cols = ['Response ID',
                     'Status',
                     'Contact ID',
                     'Legacy Comments',
                     'Comments',
                     'Referer',
                     'SessionID',
                     'User Agent',
                     'Tags',
                     'IP Address',
                     'Quota full',
                     'Total_time']
    # URL cols
    url_cols = [item for item in data.columns if "URL" in item]
    # screening questions
    screener_cols = [item for item in data.columns if 'S01Q01' in item]

    # PII cols
    pii_cols = [item for item in data.columns if ('email address' in item) or ('mobile phone number' in item)]

    col_list = unwanted_cols + url_cols + screener_cols + pii_cols

    col_list = list(set(col_list).intersection(set(data.columns.tolist())))
    data = data.drop(columns=col_list)
    return data


def clean_html_column_header(data: pd.DataFrame) -> pd.DataFrame:
    cleanr = re.compile('<.*?>')

    new_column_headers = []
    for col in data.columns:
        clean_col = re.sub(cleanr, '', col)
        new_column_headers.append(clean_col)

    data.columns = new_column_headers

    return data


def clean_other_header_issues(data: pd.DataFrame) -> pd.DataFrame:
    new_column_headers = []
    for col in data.columns:
        col = col.replace(u'\xa0', u' ')
        new_column_headers.append(col)

    data.columns = new_column_headers

    return data


def drop_inconsistent_country(data: pd.DataFrame, country_col: str, desired_countries: list) -> pd.DataFrame:
    data = data[data[country_col].isin(desired_countries)].copy(deep=True)
    return data


def replace_response_nans(data: pd.DataFrame) -> pd.DataFrame:
    time_cols = [v for v in data.columns.tolist() if ('_time' in v) or ('time_' in v)]
    dfs = []
    previous_col = None
    for t in time_cols:
        if previous_col is None:
            dfs.append(data.loc[:, : t].copy(deep=True))
        else:
            temp = data.loc[:, previous_col: t].copy(deep=True)
            temp = temp.iloc[:, 1:].copy(deep=True)
            dfs.append(temp)
        previous_col = t

    amended_dfs = []
    for df in dfs:
        for col in df:
            df[col] = ['not shown' if w is True else v for v, w in zip(df[col], pd.isna(df.iloc[:, -1]))]
        time_col = df.columns.values[-1]
        time_col_values = df[df.columns.values[-1]].tolist()
        df = df.iloc[:, : -1].fillna('not selected')
        df[time_col] = time_col_values
        amended_dfs.append(df)

    del dfs

    data = pd.concat(amended_dfs, axis=1)

    return data


def encode_AIDA_cols(data: pd.DataFrame) -> pd.DataFrame:
    return data


def drop_page_time_cols(data: pd.DataFrame) -> pd.DataFrame:
    cols = [v for v in data.columns.tolist() if ('_time' in v) or ('time_' in v)]
    data.drop(columns=cols, inplace=True)
    return data


def drop_webhook_cols(data: pd.DataFrame) -> pd.DataFrame:
    cols = [v for v in data.columns.tolist() if 'webhook' in v.lower()]
    data.drop(columns=cols, inplace=True)
    return data


def drop_attention_retention_cols(data: pd.DataFrame) -> pd.DataFrame:
    cols = [v for v in data.columns.tolist() if 'atr_' in v.lower()]
    data.drop(columns=cols, inplace=True)
    return data


def make_lowercase(text: str) -> str:
    text = text.lower()
    return text


def remove_accents(text: str) -> str:
    text = unidecode.unidecode(text)
    return text


def remove_specific_punctuations(text):
    text = text.replace('-', ' ')
    # text = text.replace("'", " ")
    text = text.replace("`", ' ')
    text = text.replace(":", ' ')
    text = text.replace(";", ' ')
    return text


def clean_spaces(text: str) -> str:
    text = re.sub('[ ]{2,}', ' ', text)  # changes more than two spaces into just one space
    text = text.strip()
    return text


def clean_text_boxes(data: pd.DataFrame, text_cols: list) -> pd.DataFrame:
    for col in text_cols:
        data[col + '_clean'] = data[col].apply(make_lowercase)
        data[col + '_clean'] = data[col].apply(remove_accents)
        data[col + '_clean'] = data[col].apply(remove_specific_punctuations)
        data[col + '_clean'] = data[col].apply(clean_spaces)

    return data


def main(df: pd.DataFrame) -> pd.DataFrame:
    df = drop_missing_ID_respondents(data=df)

    df = drop_duplicates(data=df, id_column=switch['id_column'])

    df = drop_incompletes(data=df,
                          status_col=switch['status_col'])

    df = drop_unwanted_cols(data=df)

    df = clean_html_column_header(data=df)

    df = clean_other_header_issues(data=df)

    df = drop_inconsistent_country(data=df,
                                   country_col=switch['country_col'],
                                   desired_countries=switch['desired_countries'])

    df = replace_response_nans(data=df)

    df = drop_page_time_cols(data=df)

    df = drop_webhook_cols(data=df)

    df = drop_attention_retention_cols(data=df)

    if switch['text_cols'] is None:
        pass
    else:
        df = clean_text_boxes(data=df,
                              text_cols=switch['text_cols'])

    df['wave'] = switch['wave']

    return df