def weighting_targets(gender_col: str, age_col: str, region_col: str) -> dict:
    targets = {
        "USA": [
            {
                # GENDER
                f"{gender_col}": {
                    "Male": 49.23,  # 49.23%
                    "Female": 50.77,  # 50.77%
                }
            },
            {
                # AGE
                f"{age_col}": {
                    "18-24": 11.9,  # 11.9%
                    "25-34": 17.85,  # 17.85%
                    "35-44": 16.42,  # 16.42%
                    "45-54": 16.01,  # 16.01%
                    "55-64": 16.64,  # 16.64%
                    "65 and above": 21.18,  # 21.18%
                }
            },
            {
                # REGION
                f"{region_col}": {
                    "Midwest": 20.82,  # 20.82%
                    "Northeast": 17.06,  # 17.06%
                    "South": 38.26,  # 38.26%
                    "West": 23.86,  # 23.87%
                }
            },
        ]
    }

    return targets
